﻿<%@ Page Title="Sözleşme Kayıt Ekranı" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="contract._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    
	
    <div class="row">
        <div class="col-md-12">
            <h1>Sözleşme Tanımlama Formu </h1>
			<table style="width: 50%;">
				<tr>
					<td style="font-size: x-large"><strong>Tanım Seçin:</strong></td>
					<td id="definition">
						<asp:DropDownList ID="DefinitionDropDown" runat="server">
                        </asp:DropDownList>
					</td>
					<td>
						<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DefinitionDropDown" ErrorMessage="Lütfen Bir Tanım Seçiniz" ForeColor="Red"></asp:RequiredFieldValidator>
					</td>
				</tr>
				<tr>
					<td style="font-size: x-large"><strong>Başlangıç Tarihi:</strong></td>
					<td id="contractStartDate">
						<asp:TextBox ID="txtStartDate" autocomplete="off" runat="server" CssClass="form-control" onkeypress="return false;" onpaste="return false;"></asp:TextBox>
					</td>
					<td>
						<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Başlangıç Tarihi Seçiniz" ControlToValidate="txtStartDate" ForeColor="Red"></asp:RequiredFieldValidator>
					</td>
				</tr>
				<tr>
					<td style="font-size: x-large"><strong>Bitiş Tarihi:</strong></td>
					<td id="contractEndDate">
						<asp:TextBox ID="txtEndDate" autocomplete="off" runat="server" CssClass="form-control" onkeypress="return false;" onpaste="return false;"></asp:TextBox>
					</td>
					<td>
						<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEndDate" ErrorMessage="Bitiş Tarihi Seçiniz" ForeColor="#FF3300"></asp:RequiredFieldValidator>
					</td>
				</tr>
				<tr>
					<td style="font-size: x-large"><strong>Oran:</strong></td>
					<td>
						<asp:TextBox ID="txtRatio" runat="server"></asp:TextBox>
					</td>
					<td>
						<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRatio" ErrorMessage="Sadece Sayısal Değer Giriniz" ForeColor="Red" ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
					</td>
				</tr>
				<tr>
						
					<td>
							
					</td>
					<td>
						<asp:Button ID="submitButton" runat="server" Text="Kaydet" OnClick="submitButton_Click" style="height: 100%; width: 100%" />
					</td>
				</tr>
			</table>
        </div>
    </div>
		
    <div class="row">
        <div class="col-md-12">
            <h2>Tanımlanan Sözleşmeler</h2>
			<asp:GridView ID="ContractGridView" runat="server"></asp:GridView>
		</div>
	</div>


	<script language="javascript">

		$(function () {
			$("#<%=txtStartDate.ClientID%>").datepicker({
				dateFormat: 'dd.m.yy',
				//minDate: 0,
				changeMonth: true,
				changeYear: true,
				numberOfMonths: 1,
				onSelect: function () {
					let endDateTxt = $("#<%=txtEndDate.ClientID%>");
					let startDate = $(this).datepicker('getDate');
					let minDate = $(this).datepicker('getDate');
					let endDate = endDateTxt.datepicker('getDate');
					let dateDiff = (endDate - minDate) / (86400 * 1000);

					startDate.setDate(startDate.getDate() + 30);
					if (endDate == null || dateDiff < 0) {
						endDateTxt.datepicker('setDate', minDate);
					}
					else if (dateDiff > 30) {
						endDateTxt.datepicker('setDate', startDate);
					}

					/*endDateTxt.datepicker('option', 'maxDate', startDate);*/
					endDateTxt.datepicker('option', 'minDate', minDate);

                }
			});

			$("#<%=txtEndDate.ClientID%>").datepicker({
				dateFormat: 'dd.m.yy',
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
			});
		});

    </script>


</asp:Content>
