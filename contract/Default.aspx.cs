﻿using System;
using System.Web.UI;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using contract.Models;
using System.Globalization;


namespace contract
{
    public partial class _Default : Page
    {
        string conn_string = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        contract.Models.ContractContext _db = new ContractContext();


        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                using (SqlConnection con = new SqlConnection(conn_string))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from Definitions", con);
                    DefinitionDropDown.DataSource = cmd.ExecuteReader();
                    DefinitionDropDown.DataTextField = "Description";
                    DefinitionDropDown.DataValueField = "DefinitionID";
                    DefinitionDropDown.DataBind();

                    SqlCommand listview_cmd = new SqlCommand("select tt.Description, st.StartDate, st.EndDate, st.Ratio from Contracts as st, Definitions as tt where st.DefinitionID = tt.DefinitionID; ", con);
                    SqlDataAdapter sda = new SqlDataAdapter(listview_cmd);
                    DataTable datatable = new DataTable();
                    sda.Fill(datatable);
                    ContractGridView.DataSource = datatable;
                    ContractGridView.DataBind();
                    con.Close();
                }
            }
           
        }


        protected void submitButton_Click(object sender, EventArgs e)
        {

            var definitionID = DefinitionDropDown.SelectedValue;
            string startdate = txtStartDate.Text;
            string enddate = txtEndDate.Text;
            string ratioString = !string.IsNullOrEmpty(txtRatio.Text) ? txtRatio.Text : "0";
            float ratio = float.Parse(ratioString);


            Contract contract = new Contract();
            contract.ContractID = 1;
            contract.DefinitionID = int.Parse(DefinitionDropDown.SelectedValue);
            contract.StartDate = DateTime.ParseExact(txtStartDate.Text, "dd.M.yyyy", CultureInfo.InvariantCulture);
            contract.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd.M.yyyy", CultureInfo.InvariantCulture);
            contract.Ratio = ratio;
            

            using (SqlConnection con = new SqlConnection(conn_string))
            {
                con.Open();

                SqlCommand recordsCmd = new SqlCommand("select * from Contracts", con);
                SqlDataReader reader = recordsCmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if(reader.GetInt32(1) == contract.DefinitionID) { 

                            DateTime startDate = reader.GetDateTime(2);
                            DateTime endDate = reader.GetDateTime(3);

                            int result_1 = DateTime.Compare(contract.StartDate, startDate);
                            int result_2 = DateTime.Compare(endDate, contract.StartDate);

                            if (result_1 > 0 && result_2 > 0)
                            {
                                var newEndDate = contract.StartDate;

                                System.Diagnostics.Debug.WriteLine(reader[0]);
                                System.Diagnostics.Debug.WriteLine(newEndDate);
                                SqlCommand updateCmd = new SqlCommand("UPDATE Contracts SET EndDate = CONVERT(date, '" + newEndDate.AddDays(-1) + "', 104) WHERE ContractID = " + reader[0], con);
                                updateCmd.ExecuteNonQuery();
                            }
                        }
                    }
                }

                SqlCommand insertCmd = con.CreateCommand();
                insertCmd.CommandType = System.Data.CommandType.Text;
                insertCmd.CommandText = "insert into Contracts ([DefinitionID], [StartDate], [EndDate], [Ratio]) values ( '" + contract.DefinitionID + "' ,CONVERT(date, '" + contract.StartDate + "', 104),  CONVERT(date, '" + contract.EndDate + "', 104), '" + contract.Ratio + "');";
                insertCmd.ExecuteNonQuery();
            }
            Response.Redirect(Request.Url.AbsoluteUri);
        }

    }
}