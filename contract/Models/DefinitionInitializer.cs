﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace contract.Models
{
    public class DefinitionInitializer : DropCreateDatabaseAlways<ContractContext>
    {
        protected override void Seed(ContractContext context)
        {
            GetDefinitions().ForEach(d => context.Definitions.Add(d));
        }

        private static List<Definition> GetDefinitions()
        {
            var definitions = new List<Definition>
            {
                new Definition
                {
                    DefinitionID = 1,
                    Description = "Muayene"
                },
                new Definition
                {
                    DefinitionID = 2,
                    Description = "Laboratuvar"
                },
                new Definition
                {
                    DefinitionID = 3,
                    Description = "Radyoloji"
                },
            };
            return definitions;
        }
    }
}