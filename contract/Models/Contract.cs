﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace contract.Models
{
    public class Contract
    {
        [Key]
        public int ContractID { get; set; }
        public int DefinitionID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float? Ratio { get; set; }

        public virtual Definition Definition { get; set; }
    }
}