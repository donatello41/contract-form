﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace contract.Models
{
    public class ContractContext : DbContext
    {
        public ContractContext() : base("DB")
        {
        }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Definition> Definitions { get; set; }
    }
}