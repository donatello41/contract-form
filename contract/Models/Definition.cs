﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace contract.Models
{
    public class Definition
    {
        [Key]
        public int DefinitionID { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
    }
}