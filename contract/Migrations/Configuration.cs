﻿namespace contract.Migrations
{
    using contract.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<contract.Models.ContractContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(contract.Models.ContractContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.Definitions.AddOrUpdate(
                new Definition
                {
                    DefinitionID = 1,
                    Description = "Muayene"
                },
               new Definition
               {
                   DefinitionID = 2,
                   Description = "Laboratuvar"
               },
               new Definition
               {
                   DefinitionID = 3,
                   Description = "Radyoloji"
               }
           );
            context.SaveChanges();
        }
    }
}
