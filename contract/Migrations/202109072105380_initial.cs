﻿namespace contract.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        ContractID = c.Int(nullable: false, identity: true),
                        DefinitionID = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Ratio = c.Single(),
                    })
                .PrimaryKey(t => t.ContractID)
                .ForeignKey("dbo.Definitions", t => t.DefinitionID, cascadeDelete: true)
                .Index(t => t.DefinitionID);
            
            CreateTable(
                "dbo.Definitions",
                c => new
                    {
                        DefinitionID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.DefinitionID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contracts", "DefinitionID", "dbo.Definitions");
            DropIndex("dbo.Contracts", new[] { "DefinitionID" });
            DropTable("dbo.Definitions");
            DropTable("dbo.Contracts");
        }
    }
}
